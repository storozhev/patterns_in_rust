pub mod dynam;
pub mod objects;
pub mod stat;

use dynam::SpaceObject as DynamStapceObject;
use objects::{BlackHole, InsideVisitor, NeutronStar, OutsideVisitor};
use stat::SpaceObject as StatSpaceObject;

fn main() {
    let mut neutron_star = NeutronStar::default();
    let mut black_hole = BlackHole::default();

    // static implementation
    println!("\n### static implementation ###");

    StatSpaceObject::accept(&mut neutron_star, OutsideVisitor::default());
    StatSpaceObject::accept(&mut black_hole, OutsideVisitor::default());

    println!("objects outside:\n{:?}\n{:?}", neutron_star, black_hole);

    StatSpaceObject::accept(&mut neutron_star, InsideVisitor::default());
    StatSpaceObject::accept(&mut black_hole, InsideVisitor::default());

    println!("objects inside:\n{:?}\n{:?}", neutron_star, black_hole);

    // dynamic implementation
    println!("\n### dynamic implementation ###");

    DynamStapceObject::accept(&mut neutron_star, &OutsideVisitor::default());
    DynamStapceObject::accept(&mut black_hole, &OutsideVisitor::default());

    println!("objects outside:\n{:?}\n{:?}", neutron_star, black_hole);

    DynamStapceObject::accept(&mut neutron_star, &InsideVisitor::default());
    DynamStapceObject::accept(&mut black_hole, &InsideVisitor::default());

    println!("objects inside:\n{:?}\n{:?}", neutron_star, black_hole);
}
