#[derive(Default, Debug)]
pub struct NeutronStar(pub String);

#[derive(Default, Debug)]
pub struct BlackHole(pub String);

#[derive(Default)]
pub struct InsideVisitor;

#[derive(Default)]
pub struct OutsideVisitor;
