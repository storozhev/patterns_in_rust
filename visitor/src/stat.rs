use crate::objects::{BlackHole, InsideVisitor, NeutronStar, OutsideVisitor};

pub trait Visitor {
    fn visit_neutron_star(&self, obj: &mut NeutronStar);
    fn visit_black_hole(&self, obj: &mut BlackHole);
}

impl Visitor for InsideVisitor {
    fn visit_neutron_star(&self, obj: &mut NeutronStar) {
        obj.set_info("I'm too dense because I'm made up of neutrons".into());
    }
    fn visit_black_hole(&self, obj: &mut BlackHole) {
        obj.set_info("I have the singularity inside".into());
    }
}

impl Visitor for OutsideVisitor {
    fn visit_neutron_star(&self, obj: &mut NeutronStar) {
        obj.set_info("I'm too small so you can see me only from a short distance".into());
    }
    fn visit_black_hole(&self, obj: &mut BlackHole) {
        obj.set_info("I don't reflect the light so you have no chance to see me".into());
    }
}

pub trait SpaceObject {
    fn accept<V: Visitor>(&mut self, visitor: V);
    fn set_info(&mut self, info: String);
}

impl SpaceObject for NeutronStar {
    fn accept<V: Visitor>(&mut self, visitor: V) {
        visitor.visit_neutron_star(self);
    }
    fn set_info(&mut self, info: String) {
        self.0 = info;
    }
}

impl SpaceObject for BlackHole {
    fn accept<V: Visitor>(&mut self, visitor: V) {
        visitor.visit_black_hole(self);
    }
    fn set_info(&mut self, info: String) {
        self.0 = info;
    }
}
