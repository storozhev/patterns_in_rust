use crate::objects::{BlackHole, InsideVisitor, NeutronStar, OutsideVisitor};
use std::any::Any;

pub trait Visitor {
    fn visit(&self, obj: &mut dyn SpaceObject);
}

impl Visitor for InsideVisitor {
    fn visit(&self, obj: &mut dyn SpaceObject) {
        if obj.as_any().downcast_ref::<NeutronStar>().is_some() {
            obj.set_info("I'm too dense because I'm made up of neutrons".into());
        }
        if obj.as_any().downcast_ref::<BlackHole>().is_some() {
            obj.set_info("I have the singularity inside".into());
        }
    }
}

impl Visitor for OutsideVisitor {
    fn visit(&self, obj: &mut dyn SpaceObject) {
        if obj.as_any().downcast_ref::<NeutronStar>().is_some() {
            obj.set_info("I'm too small so you can see me only from a short distance".into());
        }
        if obj.as_any().downcast_ref::<BlackHole>().is_some() {
            obj.set_info("I don't reflect the light so you have no chance to see me".into());
        }
    }
}

pub trait SpaceObject {
    fn accept(&mut self, visitor: &dyn Visitor);
    fn set_info(&mut self, info: String);
    fn as_any(&self) -> &dyn Any;
}

impl SpaceObject for NeutronStar {
    fn accept(&mut self, visitor: &dyn Visitor) {
        visitor.visit(self);
    }
    fn set_info(&mut self, info: String) {
        self.0 = info;
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl SpaceObject for BlackHole {
    fn accept(&mut self, visitor: &dyn Visitor) {
        visitor.visit(self);
    }
    fn set_info(&mut self, info: String) {
        self.0 = info;
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
}
