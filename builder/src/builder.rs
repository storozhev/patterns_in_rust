#[derive(PartialEq, Debug)]
pub struct MusicAlbum<'a> {
    pub author: &'a str,
    pub name: &'a str,
    pub year: u32,
}

#[derive(Default)]
pub struct MusicAlbumBuilder<'a> {
    author: &'a str,
    name: &'a str,
    year: u32,
}

impl<'a> MusicAlbumBuilder<'a> {
    pub fn new() -> Self {
        MusicAlbumBuilder::default()
    }

    pub fn with_author(mut self, author: &'a str) -> Self {
        self.author = author;
        self
    }

    pub fn with_name(mut self, name: &'a str) -> Self {
        self.name = name;
        self
    }

    pub fn with_year(mut self, year: u32) -> Self {
        self.year = year;
        self
    }

    pub fn build(self) -> MusicAlbum<'a> {
        MusicAlbum {
            author: self.author,
            name: self.name,
            year: self.year,
        }
    }
}
