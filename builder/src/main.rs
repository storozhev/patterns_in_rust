mod builder;

use builder::{MusicAlbum, MusicAlbumBuilder};

fn main() {
    let music_album = MusicAlbumBuilder::new()
        .with_author("Metallica")
        .with_name("ReLoad")
        .with_year(1997)
        .build();

    assert_eq!(
        MusicAlbum {
            author: "Metallica",
            name: "ReLoad",
            year: 1997,
        },
        music_album,
    );
}
