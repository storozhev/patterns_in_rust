use std::cell::{Cell, UnsafeCell};
use std::ops::{Deref, DerefMut};

pub struct Lock<T: ?Sized> {
    locked: Cell<bool>,
    data: UnsafeCell<T>,
}

impl<T> Lock<T> {
    pub fn new(data: T) -> Self {
        Self {
            data: UnsafeCell::new(data),
            locked: Cell::new(false),
        }
    }
}

impl<T: ?Sized> Lock<T> {
    pub fn lock(&self) -> LockGuard<T> {
        // for simplicity
        while self.locked.get() {
            std::thread::sleep(std::time::Duration::from_micros(1));
        }

        self.locked.set(true);
        LockGuard { lock: self }
    }

    fn unlock(&self) {
        self.locked.set(false);
    }
}

unsafe impl<T: ?Sized + Send> Sync for Lock<T> {}

pub struct LockGuard<'a, T: ?Sized + 'a> {
    lock: &'a Lock<T>,
}

impl<T: ?Sized> Deref for LockGuard<'_, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.lock.data.get() }
    }
}

impl<T: ?Sized> DerefMut for LockGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.lock.data.get() }
    }
}

impl<T: ?Sized> Drop for LockGuard<'_, T> {
    fn drop(&mut self) {
        self.lock.unlock();
    }
}
