mod lockguard;

use lockguard::Lock;
use std::sync::Arc;
use std::thread;

#[derive(Default)]
struct Foo {
    pub counter: u8,
}

fn main() {
    let foo_instance = Arc::new(Lock::new(Foo::default()));
    let mut join_handles = Vec::with_capacity(10);

    for _ in 0..10 {
        let foo_clone = Arc::clone(&foo_instance);
        let jh = thread::spawn(move || {
            let mut foo_lock_guard = foo_clone.lock();
            foo_lock_guard.counter += 1;
        });

        join_handles.push(jh);
    }

    join_handles.into_iter().for_each(|jh| jh.join().unwrap());

    assert_eq!(10, foo_instance.lock().counter);
}
